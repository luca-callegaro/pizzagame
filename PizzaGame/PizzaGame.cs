using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace PizzaGame
{
    public partial class PizzaGame : Form
    {
        static readonly Random _random = new Random();
        private int _numberOfPizzas;
        private List<Player> _players = new List<Player>(2);
        public PizzaGame()
        {
            InitializeComponent();
        }
        private void StartGame()
        {
            InitPlayers();
            _numberOfPizzas = GenerateRandomNumberOfPizzas();
            lblNumPizze.Text = _numberOfPizzas.ToString();
            UpdateDataGridView();
        }
        private void UpdateDataGridView()
        {
            dataGridViewPizze.Rows.Clear();
            dataGridViewPizze.Rows.Add(_numberOfPizzas);
        }
        private void ChangeTurnPlayer(int eatedPizza)
        {
            _players.ForEach(x => x.ActivePlayer = !x.ActivePlayer);
            cmdP1P1.Enabled = _players[0].ActivePlayer && eatedPizza != 1;
            cmdP1P2.Enabled = _players[0].ActivePlayer && eatedPizza != 2;
            cmdP1P3.Enabled = _players[0].ActivePlayer && eatedPizza != 3;
            cmdP2P1.Enabled = _players[1].ActivePlayer && eatedPizza != 1;
            cmdP2P2.Enabled = _players[1].ActivePlayer && eatedPizza != 2;
            cmdP2P3.Enabled = _players[1].ActivePlayer && eatedPizza != 3;
        }
        private void PlayTurn(int choice)
        {
            if (_numberOfPizzas < choice)
            {
                MessageBox.Show($"There aren't any pizzas left!");
                return;
            }
            _numberOfPizzas -= choice;
            lblNumPizze.Text = _numberOfPizzas.ToString();
            if (_numberOfPizzas == 0)
            {
                var DialogResult = new CustomMessageBox($"Player {_players.Find(x => x.ActivePlayer).Name} is dead!", "Bad News").ShowDialog();
                switch (DialogResult)
                {
                    case DialogResult.Retry:
                        cmdP1P1.Enabled = true;
                        cmdP1P2.Enabled = true;
                        cmdP1P3.Enabled = true;
                        cmdP2P1.Enabled = false;
                        cmdP2P2.Enabled = false;
                        cmdP2P3.Enabled = false;
                        StartGame();
                        break;
                    case DialogResult.Cancel:
                        this.Close();
                        break;
                }
                return;
            }
            if (_numberOfPizzas == 1 && _numberOfPizzas == choice)
            {
                cmdChangeTurn.Enabled = true;
            }
            ChangeTurnPlayer(choice);
            UpdateDataGridView();
        }
        private void InitPlayers()
        {
            _players.Clear();
            _players.Add(new Player() { ActivePlayer = true, Name = Interaction.InputBox("What's your Name?", "First player name", "A") });
            _players.Add(new Player() { ActivePlayer = false, Name = Interaction.InputBox("What's your Name?", "Second player name", "B") });
            lblP1Name.Text = _players[0].Name;
            lblP2Name.Text = _players[1].Name;
        }
        private static int GenerateRandomNumberOfPizzas()
        {
            return _random.Next(11, 20);
        }
        private void cmdP1P1_Click(object sender, EventArgs e)
        {
            PlayTurn(1);
        }
        private void cmdP1P2_Click(object sender, EventArgs e)
        {
            PlayTurn(2);
        }
        private void cmdP1P3_Click(object sender, EventArgs e)
        {
            PlayTurn(3);
        }
        private void cmdP2P1_Click(object sender, EventArgs e)
        {
            PlayTurn(1);
        }
        private void cmdP2P2_Click(object sender, EventArgs e)
        {
            PlayTurn(2);
        }
        private void cmdP2P3_Click(object sender, EventArgs e)
        {
            PlayTurn(3);
        }
        private void cmdChangeTurn_Click(object sender, EventArgs e)
        {
            PlayTurn(0);
            cmdChangeTurn.Enabled = false;
        }
        private void dataGridViewPizzas_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex == dataGridViewPizze.Rows.Count - 1)
            {
                e.Value = Properties.Resources.pizzaDellaMorte;
                e.FormattingApplied = true;
            }
        }
        private void PizzaGame_Load(object sender, EventArgs e)
        {
            Size = new Size(800, 600);
            StartPosition = FormStartPosition.CenterScreen;
        }
        private void PizzaGame_Shown(object sender, EventArgs e)
        {
            StartGame();
        }
    }
}