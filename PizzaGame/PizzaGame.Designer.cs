﻿namespace PizzaGame
{
    partial class PizzaGame
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PizzaGame));
            cmdP1P1 = new Button();
            cmdP1P2 = new Button();
            cmdP1P3 = new Button();
            cmdP2P3 = new Button();
            cmdP2P2 = new Button();
            cmdP2P1 = new Button();
            label1 = new Label();
            lblNumPizze = new Label();
            lblP1Name = new Label();
            lblP2Name = new Label();
            dataGridViewPizze = new DataGridView();
            Column1 = new DataGridViewImageColumn();
            cmdChangeTurn = new Button();
            ((System.ComponentModel.ISupportInitialize)dataGridViewPizze).BeginInit();
            SuspendLayout();
            // 
            // cmdP1P1
            // 
            cmdP1P1.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdP1P1.Location = new Point(58, 133);
            cmdP1P1.Name = "cmdP1P1";
            cmdP1P1.Size = new Size(91, 98);
            cmdP1P1.TabIndex = 0;
            cmdP1P1.Text = "Eats 1 Pizza";
            cmdP1P1.UseVisualStyleBackColor = true;
            cmdP1P1.Click += cmdP1P1_Click;
            // 
            // cmdP1P2
            // 
            cmdP1P2.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdP1P2.Location = new Point(58, 256);
            cmdP1P2.Name = "cmdP1P2";
            cmdP1P2.Size = new Size(91, 98);
            cmdP1P2.TabIndex = 1;
            cmdP1P2.Text = "Eats 2 Pizzas";
            cmdP1P2.UseVisualStyleBackColor = true;
            cmdP1P2.Click += cmdP1P2_Click;
            // 
            // cmdP1P3
            // 
            cmdP1P3.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdP1P3.Location = new Point(58, 379);
            cmdP1P3.Name = "cmdP1P3";
            cmdP1P3.Size = new Size(91, 98);
            cmdP1P3.TabIndex = 2;
            cmdP1P3.Text = "Eats 3 Pizzas";
            cmdP1P3.UseVisualStyleBackColor = true;
            cmdP1P3.Click += cmdP1P3_Click;
            // 
            // cmdP2P3
            // 
            cmdP2P3.Enabled = false;
            cmdP2P3.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdP2P3.Location = new Point(667, 379);
            cmdP2P3.Name = "cmdP2P3";
            cmdP2P3.Size = new Size(91, 98);
            cmdP2P3.TabIndex = 5;
            cmdP2P3.Text = "Eats 3 Pizzas";
            cmdP2P3.UseVisualStyleBackColor = true;
            cmdP2P3.Click += cmdP2P3_Click;
            // 
            // cmdP2P2
            // 
            cmdP2P2.Enabled = false;
            cmdP2P2.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdP2P2.Location = new Point(667, 256);
            cmdP2P2.Name = "cmdP2P2";
            cmdP2P2.Size = new Size(91, 98);
            cmdP2P2.TabIndex = 4;
            cmdP2P2.Text = "Eats 2 Pizzas";
            cmdP2P2.UseVisualStyleBackColor = true;
            cmdP2P2.Click += cmdP2P2_Click;
            // 
            // cmdP2P1
            // 
            cmdP2P1.Enabled = false;
            cmdP2P1.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdP2P1.Location = new Point(667, 133);
            cmdP2P1.Name = "cmdP2P1";
            cmdP2P1.Size = new Size(91, 98);
            cmdP2P1.TabIndex = 3;
            cmdP2P1.Text = "Eats 1 Pizza ";
            cmdP2P1.UseVisualStyleBackColor = true;
            cmdP2P1.Click += cmdP2P1_Click;
            // 
            // label1
            // 
            label1.Font = new Font("Tempus Sans ITC", 12F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(333, 9);
            label1.Name = "label1";
            label1.Size = new Size(149, 38);
            label1.TabIndex = 6;
            label1.Text = "Remaining Pizzas";
            label1.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblNumPizze
            // 
            lblNumPizze.Font = new Font("Tempus Sans ITC", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblNumPizze.Location = new Point(333, 47);
            lblNumPizze.Name = "lblNumPizze";
            lblNumPizze.Size = new Size(149, 39);
            lblNumPizze.TabIndex = 7;
            lblNumPizze.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblP1Name
            // 
            lblP1Name.Font = new Font("Tempus Sans ITC", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblP1Name.Location = new Point(58, 82);
            lblP1Name.Name = "lblP1Name";
            lblP1Name.Size = new Size(91, 25);
            lblP1Name.TabIndex = 8;
            lblP1Name.Text = "Player 1";
            lblP1Name.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // lblP2Name
            // 
            lblP2Name.Font = new Font("Tempus Sans ITC", 12F, FontStyle.Bold, GraphicsUnit.Point);
            lblP2Name.Location = new Point(667, 82);
            lblP2Name.Name = "lblP2Name";
            lblP2Name.Size = new Size(91, 25);
            lblP2Name.TabIndex = 9;
            lblP2Name.Text = "Player 2";
            lblP2Name.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // dataGridViewPizze
            // 
            dataGridViewPizze.AllowUserToAddRows = false;
            dataGridViewPizze.AllowUserToDeleteRows = false;
            dataGridViewPizze.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewPizze.Columns.AddRange(new DataGridViewColumn[] { Column1 });
            dataGridViewPizze.Location = new Point(351, 174);
            dataGridViewPizze.Name = "dataGridViewPizze";
            dataGridViewPizze.ReadOnly = true;
            dataGridViewPizze.RowHeadersVisible = false;
            dataGridViewPizze.RowTemplate.Height = 100;
            dataGridViewPizze.Size = new Size(110, 363);
            dataGridViewPizze.TabIndex = 10;
            dataGridViewPizze.CellFormatting += dataGridViewPizzas_CellFormatting;
            // 
            // Column1
            // 
            Column1.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            Column1.HeaderText = "Pizzas";
            Column1.Image = (Image)resources.GetObject("Column1.Image");
            Column1.Name = "Column1";
            Column1.ReadOnly = true;
            // 
            // cmdChangeTurn
            // 
            cmdChangeTurn.Enabled = false;
            cmdChangeTurn.Font = new Font("Tempus Sans ITC", 11.25F, FontStyle.Bold, GraphicsUnit.Point);
            cmdChangeTurn.Location = new Point(351, 89);
            cmdChangeTurn.Name = "cmdChangeTurn";
            cmdChangeTurn.Size = new Size(110, 69);
            cmdChangeTurn.TabIndex = 11;
            cmdChangeTurn.Text = "Change Turn";
            cmdChangeTurn.UseVisualStyleBackColor = true;
            cmdChangeTurn.Click += cmdChangeTurn_Click;
            // 
            // PizzaGame
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(784, 561);
            Controls.Add(cmdChangeTurn);
            Controls.Add(dataGridViewPizze);
            Controls.Add(lblP2Name);
            Controls.Add(lblP1Name);
            Controls.Add(lblNumPizze);
            Controls.Add(label1);
            Controls.Add(cmdP2P3);
            Controls.Add(cmdP2P2);
            Controls.Add(cmdP2P1);
            Controls.Add(cmdP1P3);
            Controls.Add(cmdP1P2);
            Controls.Add(cmdP1P1);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "PizzaGame";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Pizza Game";
            Load += PizzaGame_Load;
            Shown += PizzaGame_Shown;
            ((System.ComponentModel.ISupportInitialize)dataGridViewPizze).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Button cmdP1P1;
        private Button cmdP1P2;
        private Button cmdP1P3;
        private Button cmdP2P3;
        private Button cmdP2P2;
        private Button cmdP2P1;
        private Label label1;
        private Label lblNumPizze;
        private Label lblP1Name;
        private Label lblP2Name;
        private DataGridView dataGridViewPizze;
        private DataGridViewImageColumn Column1;
        private Button cmdChangeTurn;
    }
}