﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaGame
{
    public class Player
    {
        public string Name { get; set; }
        public bool ActivePlayer { get; set; }
    }
}
