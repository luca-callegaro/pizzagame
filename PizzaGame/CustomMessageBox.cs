﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaGame
{
    public class CustomMessageBox : Form
    {
        public CustomMessageBox(string message, string caption)
        {
            Text = caption;
            Size = new Size(300, 150);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            StartPosition = FormStartPosition.CenterScreen;
            Label label = new Label
            {
                Text = message,
                Location = new Point(10, 10),
                AutoSize = true
            };
            Controls.Add(label);

            Button buttonCustom = new Button
            {
                Text = "Retry",
                Location = new Point(50, 50)
            };
            buttonCustom.Click += (sender, e) =>
            {
                DialogResult = DialogResult.Retry;
            };
            Controls.Add(buttonCustom);

            Button buttonClose = new Button
            {
                Text = "Close",
                Location = new Point(150, 50)
            };
            buttonClose.Click += (sender, e) =>
            {
                DialogResult = DialogResult.Cancel;
            };
            Controls.Add(buttonClose);

            AcceptButton = buttonCustom;
            CancelButton = buttonClose;
        }
    }
}
